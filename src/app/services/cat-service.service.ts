import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  API_KEY,
  API_SEARCH,
  API_SEARCH_BREED,
  API_SEARCH_CATEGORIES,
  API_SEARCH_LIMIT,
  GET_BREEDS,
  GET_BY_ID,
  GET_CATEGORIES,
  GET_URL_INIT,
} from '../core/constansts';

@Injectable({
  providedIn: 'root',
})
export class CatServiceService {
  limitInit = '15';
  constructor(private http: HttpClient) {}

  public get(limit: string = this.limitInit): Observable<any> {
    return this.http
      .get(`${GET_URL_INIT}${limit}${API_KEY}`)
      .pipe(map((res) => res));
  }
  public getDetailById(catId: string): Observable<any> {
    return this.http.get(`${GET_BY_ID}${catId}`).pipe(map((res) => res));
  }
  public getBreeds(): Observable<any> {
    return this.http.get(`${GET_BREEDS}`).pipe(map((res) => res));
  }
  public getCategories(): Observable<any> {
    return this.http.get(`${GET_CATEGORIES}`).pipe(map((res) => res));
  }
  isNumber = (n: any) => {
    return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
  };
  public filterCats(
    limit: string = this.limitInit,
    breedsId: string = '',
    categoryId: string = ''
  ): Observable<any> {
    if (!this.isNumber(limit)) limit = this.limitInit;
    return this.http
      .get(
        `${API_SEARCH}${API_SEARCH_LIMIT}${limit}${API_SEARCH_BREED}${breedsId}${API_SEARCH_CATEGORIES}${categoryId}${API_KEY}`
      )
      .pipe(map((res) => res));
  }
}
