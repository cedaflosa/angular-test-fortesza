import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Detail } from 'src/app/core/models/cat.interface';
import { CatServiceService } from 'src/app/services/cat-service.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit, OnDestroy {
  catSubs: Subscription;
  catDetail: Detail;

  constructor(
    private activedRouter: ActivatedRoute,
    private api: CatServiceService
  ) {}

  ngOnInit(): void {
    this.activedRouter.params.subscribe((params) => {
      const id = params['id'];
      this.getCatDetails(id);
    });
  }

  ngOnDestroy(): void {
    if (this.catSubs) this.catSubs.unsubscribe();
  }

  getCatDetails = (catId: string) => {
    if (this.catSubs) this.catSubs.unsubscribe();
    this.catSubs = this.api.getDetailById(catId).subscribe((res) => {
      this.catDetail = res;
    });
  };
}
