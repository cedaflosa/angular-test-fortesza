import { Component, OnInit } from '@angular/core';
import { CatServiceService } from '../../services/cat-service.service';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Breeds, Cat, Category } from 'src/app/core/models/cat.interface';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  cats: Cat[];
  limitCatSubs: Subscription;
  constructor(private api: CatServiceService) {}

  ngOnInit() {
    this.getCatsWithInitLimit();
  }

  getCatsWithInitLimit(limit = this.api.limitInit) {
    if (this.limitCatSubs) this.limitCatSubs.unsubscribe();
    this.limitCatSubs = this.api.get(limit).subscribe((res) => {
      this.cats = res;
    });
  }
  filterCats = (cats: Cat[]) => {
    this.cats = cats;
  };
}
