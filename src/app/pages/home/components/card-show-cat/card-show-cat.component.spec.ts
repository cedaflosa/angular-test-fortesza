import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { RouterTestingModule } from '@angular/router/testing';
import { modules } from 'src/app/app.module';
import { Cat } from 'src/app/core/models/cat.interface';

import { CardShowCatComponent } from './card-show-cat.component';

describe('CardShowCatComponent', () => {
  let component: CardShowCatComponent;
  let fixture: ComponentFixture<CardShowCatComponent>;
  let cat: Cat = {
    id: 'xedqe',
    url: 'dedd',
    breeds: [],
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CardShowCatComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, ...modules],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardShowCatComponent);
    component = fixture.componentInstance;
    component.cat = cat;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should object cat is defined', () => {
    expect(component.cat).toBeDefined();
  });

  it('should object cat id exist', () => {
    expect(component.cat.id).toBe('xedqe');
  });

  it('should object cat undefined property', () => {
    expect(component.cat.width).toBeUndefined();
  });
});
