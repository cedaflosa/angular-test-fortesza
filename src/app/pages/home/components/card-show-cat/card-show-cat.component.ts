import { Component, Input } from '@angular/core';
import { Cat } from 'src/app/core/models/cat.interface';

@Component({
  selector: 'app-card-show-cat',
  templateUrl: './card-show-cat.component.html',
  styleUrls: ['./card-show-cat.component.scss'],
})
export class CardShowCatComponent {
  @Input() cat: Cat;
}
