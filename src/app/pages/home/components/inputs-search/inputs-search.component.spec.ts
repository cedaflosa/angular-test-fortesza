import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { modules } from 'src/app/app.module';

import { InputsSearchComponent } from './inputs-search.component';
describe('InputsSearchComponent', () => {
  let component: InputsSearchComponent;
  let fixture: ComponentFixture<InputsSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [InputsSearchComponent],
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ...modules,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
      ],
      providers: [HttpClient],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputsSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should object cats undefined', () => {
    expect(component.cats).toBeUndefined();
  });

  it('should object breeds undefined', () => {
    expect(component.breeds).toBeUndefined();
  });

  it('should object categories undefined', () => {
    expect(component.categories).toBeUndefined();
  });
  it('should object origins undefined', () => {
    expect(component.origins).toBeUndefined();
  });
});
