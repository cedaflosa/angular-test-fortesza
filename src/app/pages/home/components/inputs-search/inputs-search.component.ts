import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Breeds, Cat, Category } from 'src/app/core/models/cat.interface';
import { CatServiceService } from 'src/app/services/cat-service.service';

@Component({
  selector: 'app-inputs-search',
  templateUrl: './inputs-search.component.html',
  styleUrls: ['./inputs-search.component.scss'],
})
export class InputsSearchComponent implements OnInit {
  @Output() catsEvemitter = new EventEmitter<Cat[]>();

  valueLimit = new FormControl();
  breedControl = new FormControl();
  categoriesControl = new FormControl();
  originsControl = new FormControl();

  cats: Cat[];
  breeds: Breeds[];
  breedsOrigin: Breeds[];
  categories: Category[];
  origins: string[];

  breedSubs: Subscription;
  categoriesSubs: Subscription;
  filterCatsSubs: Subscription;

  limitSelected: string = '';
  breedSelected: string = '';
  categorySelected: string = '';
  originSelected: string = '';
  constructor(private api: CatServiceService) {}

  ngOnInit(): void {
    this.getBreeds();
    this.getCategories();

    this.breedControl.setValue('');
    this.originsControl.setValue('');
    this.categoriesControl.setValue('');
    this.valueLimit.setValue('');

    this.valueLimit.valueChanges.subscribe((limit) => {
      this.limitSelected = limit;
      this.filterCats();
    });

    this.breedControl.valueChanges.subscribe((breed) => {
      this.breedSelected = breed.id;
      this.filterCats();
    });
    this.categoriesControl.valueChanges.subscribe((category) => {
      this.categorySelected = category.id;
      this.breedControl.setValue('');
      this.originsControl.setValue('');
      this.filterCats();
    });
    this.originsControl.valueChanges.subscribe((origin) => {
      this.originSelected = origin;
      this.breeds = this.breedsOrigin;
      if (this.originSelected !== '') {
        const breedsbyOrigin = this.breeds.filter((breed) => {
          return breed.origin === this.originSelected;
        });
        this.breeds = breedsbyOrigin;
      }
      this.breedControl.setValue('');
      this.filterCats();
    });
  }

  onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  getOrigins() {
    this.origins = this.breeds.map((breed) => {
      return breed.origin;
    });
    this.origins = this.origins.filter(this.onlyUnique);
  }

  getBreeds = () => {
    if (this.breedSubs) this.breedSubs.unsubscribe();
    this.breedSubs = this.api.getBreeds().subscribe((res) => {
      this.breeds = res;
      this.breedsOrigin = this.breeds;
      this.getOrigins();
    });
  };

  getCategories = () => {
    if (this.categoriesSubs) this.categoriesSubs.unsubscribe();
    this.categoriesSubs = this.api.getCategories().subscribe((res) => {
      this.categories = res;
    });
  };
  filterCats = () => {
    if (this.filterCatsSubs) this.filterCatsSubs.unsubscribe();
    this.filterCatsSubs = this.api
      .filterCats(this.limitSelected, this.breedSelected, this.categorySelected)
      .subscribe((res) => {
        this.cats = res;
        this.catsEvemitter.emit(this.cats);
      });
  };
}
