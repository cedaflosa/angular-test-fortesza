import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { DetailComponent } from './pages/detail/detail.component';
import { CardShowCatComponent } from './pages/home/components/card-show-cat/card-show-cat.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectFilterModule } from 'mat-select-filter';
import { InputsSearchComponent } from './pages/home/components/inputs-search/inputs-search.component';
import { MatFormFieldModule } from '@angular/material/form-field';

export const modules = [
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatSelectFilterModule,
];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DetailComponent,
    CardShowCatComponent,
    InputsSearchComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    ...modules,
  ],
  exports: [...modules],
  providers: [HttpClient],
  bootstrap: [AppComponent],
})
export class AppModule {}
