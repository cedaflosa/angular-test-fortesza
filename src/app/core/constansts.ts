export const GET_URL_INIT = 'https://api.thecatapi.com/v1/images/search?limit=';
export const GET_BREEDS = 'https://api.thecatapi.com/v1/breeds';
export const GET_CATEGORIES = 'https://api.thecatapi.com/v1/categories';
export const GET_BY_ID = 'https://api.thecatapi.com/v1/images/';
export const API_SEARCH = 'https://api.thecatapi.com/v1/images/search';
export const API_SEARCH_LIMIT = '?limit=';
export const API_SEARCH_BREED = '&breed_ids=';
export const API_SEARCH_CATEGORIES = '&category_ids=';
export const API_KEY =
  '&api_key=live_Royzj9A51yXLbDzU1yr1yq4RUoyw423cFFX4LMyhVUMzSUlLmAlixZmJSmUHwVCF';
