export type Category = {
  id: number;
  name: string;
};

export type Breeds = {
  id: string;
  name: string;
  origin: string;
  temperament: string;
  weight: string;
  description: string;
};

export type Detail = {
  categories: Category[];
  height: number;
  id: string;
  url: string;
  width: number;
};

export type Cat = {
  breeds?: Breeds[];
  height?: number;
  id: string;
  url: string;
  width?: number;
};

